package ibm.mq.Concurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConcurrenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConcurrenciaApplication.class, args);
	}

}
