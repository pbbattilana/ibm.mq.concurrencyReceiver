package ibm.mq.Concurrency;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Listener {
    
    @JmsListener(
            destination = "${con.queueName}",
            containerFactory = "queueFactory",
            concurrency = "3"
    )
    public void receiver(String msg){
        UUID.randomUUID();
        log.info("Message Received: " + msg);
    }
}
